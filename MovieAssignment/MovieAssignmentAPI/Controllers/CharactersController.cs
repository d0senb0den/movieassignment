﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAssignmentAPI.Data;
using MovieAssignmentAPI.DTO.CharacterDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieAssignmentContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieAssignmentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Recieves all characters.
        /// </summary>
        /// <returns>A list of characters or a response status code.</returns>
        [HttpGet] // GET: api/Characters
        public async Task<ActionResult<IEnumerable<DTO_ReadCharacter>>> GetCharacters()
        {
          if (_context.Characters == null)
          {
              return NotFound();
          }
            return _mapper.Map<List< DTO_ReadCharacter>>(await _context.Characters.Include(character => character.Movies).ToListAsync());
        }

        /// <summary>
        /// Recieves a specific character based on ID.
        /// </summary>
        /// <param name="id">Character ID.</param>
        /// <returns>The specified character or a response status code.</returns>
        [HttpGet("{id}")] // GET: api/Characters/5
        public async Task<ActionResult<DTO_ReadCharacter>> GetCharacter(int id)
        {
          if (_context.Characters == null)
          {
              return NotFound();
          }
            var character = await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadCharacter>(character);
        }

        /// <summary>
        /// Edits a specific character based on ID.
        /// </summary>
        /// <param name="id">Character ID.</param>
        /// <param name="character">The specified character.</param>
        /// <returns>Response status code</returns>
        [HttpPut("{id}")] // PUT: api/Characters/5
        public async Task<IActionResult> PutCharacter(int id, DTO_EditCharacter character)
        {
            if (id != character.DTO_Id)
            {
                return BadRequest();
            }

            Character oldCharacter = _context.Characters.Include(c => c.Movies).Single(c => c.Id == character.DTO_Id);

            List<Movie> tempMovies = new();

            foreach (int movieId in character.Movies)
            {
                if (await _context.Movies.FindAsync(movieId) == null)
                {
                    return BadRequest("Specified movie does not exist");
                }
                else
                {
                    Movie movie = await _context.Movies.FindAsync(movieId);
                    tempMovies.Add(movie);
                }
            }

            oldCharacter.Movies = tempMovies;
            oldCharacter.Name = character.Name;
            oldCharacter.Alias = character.Alias;
            oldCharacter.Gender = character.Gender;
            oldCharacter.ImageUrl = character.ImageUrl;

            _context.Update(oldCharacter);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a character based on input from body in swagger.
        /// </summary>
        /// <param name="characterDTO">Character data.</param>
        /// <returns>Response status code for creating a character or errors.</returns>
        [HttpPost] // POST: api/Characters
        public async Task<ActionResult<DTO_CreateCharacter>> PostCharacter(DTO_CreateCharacter characterDTO)
        {
          if (_context.Characters == null)
          {
              return Problem("Entity set 'MovieAssignmentContext.Characters'  is null.");
          }
          Character character = _mapper.Map<Character>(characterDTO);

            List<Movie> movies = new();

            foreach (int movieId in characterDTO.Movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    return BadRequest("Specified movied does not exist");
                }
                movies.Add(movie);
            }

            character.Movies = movies;

            await _context.Characters.AddAsync(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, characterDTO);
        }

        /// <summary>
        /// Removes a specific character based on ID.
        /// </summary>
        /// <param name="id">Character ID.</param>
        /// <returns>Response status code</returns>
        [HttpDelete("{id}")] // DELETE: api/Characters/5
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return (_context.Characters?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}

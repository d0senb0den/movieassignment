﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.DTO.FranchiseDTO
{
    public class DTO_EditFranchise
    {
        public int DTO_Id { get; set; }

        [StringLength(50)]
        public string? Name { get; set; }

        [StringLength(300)]
        public string? Description { get; set; }

        public List<int>? Movies { get; set; }
    }
}

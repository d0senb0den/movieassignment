﻿namespace MovieAssignmentAPI.DTO.FranchiseDTO
{
    public class DTO_ReadFranchise
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public List<int>? Movies { get; set; }
    }
}

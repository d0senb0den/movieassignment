﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Data
{
    public class MovieAssignmentContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }


        private readonly IConfiguration _configuration;
        public MovieAssignmentContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbConn = _configuration.GetSection("ConnectionStrings").GetSection("MovieAssignmentDB").Value;

            SqlConnectionStringBuilder connectionStringBuilder = new()
            {
                DataSource = dbConn,
                InitialCatalog = "MovieAssignmentDB",
                IntegratedSecurity = true,
                Encrypt = false
                
            };
            optionsBuilder.UseSqlServer(connectionStringBuilder.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise {
                Id = 1,
                Name = "Kill Bill",
                Description = "A Bloody action series involving martial arts"
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie { 
                Id = 1,
                Title = "Kill Bill",
                Director = "Quentin Tarantino",
                Genre = "Action",
                ReleaseDate = new DateTime(2003,12,03),
                FranchiseId = 1,
                ImageUrl = "https://www.imdb.com/title/tt0266697/mediaviewer/rm48438785/?ref_=tt_ov_i",
                TrailerUrl = "https://www.imdb.com/video/vi3102711321/?playlistId=tt0266697&ref_=tt_pr_ov_vi"
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character {
                Id = 1,
                Name = "Beatrix",
                Alias = "Black Mamba",
                Gender = "Female",
                ImageUrl = "https://www.imdb.com/title/tt0266697/mediaviewer/rm2186293248/?ref_=tt_md_3"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                Id = 2,
                Name = "Bill",
                Alias = "Snake Charmer",
                Gender = "Male",
                ImageUrl = "https://www.imdb.com/title/tt0266697/mediaviewer/rm3629816064?ft0=name&fv0=nm0001016&ft1=image_type&fv1=still_frame&ref_=tt_ch"
                });


          
            
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "I Am Legend",
                    Description = "Post apocalyptic world in which our portagonist has to try and survive amongst the mutated monsters"
                });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                Id = 2,
                Title = "I Am Legend",
                Director = "Francis Lawrence",
                Genre = "Apocalyptic and post-apocalyptic fiction",
                ReleaseDate= new DateTime(2008,01,25),
                FranchiseId = 2,
                ImageUrl = "https://www.imdb.com/title/tt0480249/mediaviewer/rm2203650560/?ref_=tt_ov_i",
                TrailerUrl = "https://www.imdb.com/video/vi3820028185/?playlistId=tt0480249&ref_=tt_pr_ov_vi"
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 3,
                    Name = "Robert Neville",
                    Alias = "Dr. Robert Neville",
                    Gender = "Male",
                    ImageUrl = "https://www.imdb.com/title/tt0480249/mediaviewer/rm1077319680?ft0=name&fv0=nm0000226&ft1=image_type&fv1=still_frame&ref_=tt_ch"
                });

           
            
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 3,
                    Name = "Scary Movie",
                    Description = "Comedic parodies on classic horror movies"
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    Title = "Scary Movie 2",
                    Director = "Keenen Ivory Wayans",
                    Genre = "Horror-Comedy",
                    ReleaseDate = new DateTime(2001,07,04),
                    FranchiseId = 3,
                    ImageUrl = "https://www.imdb.com/title/tt0257106/mediaviewer/rm699865088/?ref_=tt_ov_i",
                    TrailerUrl = "https://www.imdb.com/video/vi852532761/?playlistId=tt0257106&ref_=tt_ov_vi"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 4,
                    Name = "Cindy",
                    Alias = "",
                    Gender = "Female",
                    ImageUrl = "https://www.imdb.com/title/tt0257106/mediaviewer/rm12561409?ft0=name&fv0=nm0267506&ft1=image_type&fv1=still_frame&ref_=tt_ch"
                });

            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(b => b.Movies)
                .UsingEntity<Dictionary<string,object>>(
                configureRight => configureRight.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                configureLeft => configureLeft.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                joiningEntities =>
                {
                    joiningEntities.HasKey("MovieId", "CharacterId");
                    joiningEntities.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 2, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 4 }
                        );
                }); 
        }
    }
}

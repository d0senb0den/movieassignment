﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieAssignmentAPI.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ImageUrl = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FranchiseId = table.Column<int>(type: "int", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ReleaseDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ImageUrl = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    TrailerUrl = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "ImageUrl", "Name" },
                values: new object[,]
                {
                    { 1, "Black Mamba", "Female", "https://www.imdb.com/title/tt0266697/mediaviewer/rm2186293248/?ref_=tt_md_3", "Beatrix" },
                    { 2, "Snake Charmer", "Male", "https://www.imdb.com/title/tt0266697/mediaviewer/rm3629816064?ft0=name&fv0=nm0001016&ft1=image_type&fv1=still_frame&ref_=tt_ch", "Bill" },
                    { 3, "Dr. Robert Neville", "Male", "https://www.imdb.com/title/tt0480249/mediaviewer/rm1077319680?ft0=name&fv0=nm0000226&ft1=image_type&fv1=still_frame&ref_=tt_ch", "Robert Neville" },
                    { 4, "", "Female", "https://www.imdb.com/title/tt0257106/mediaviewer/rm12561409?ft0=name&fv0=nm0267506&ft1=image_type&fv1=still_frame&ref_=tt_ch", "Cindy" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "A Bloody action series involving martial arts", "Kill Bill" },
                    { 2, "Post apocalyptic world in which our portagonist has to try and survive amongst the mutated monsters", "I Am Legend" },
                    { 3, "Comedic parodies on classic horror movies", "Scary Movie" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "ImageUrl", "ReleaseDate", "Title", "TrailerUrl" },
                values: new object[] { 1, "Quentin Tarantino", 1, "Action", "https://www.imdb.com/title/tt0266697/mediaviewer/rm48438785/?ref_=tt_ov_i", new DateTime(2003, 12, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kill Bill", "https://www.imdb.com/video/vi3102711321/?playlistId=tt0266697&ref_=tt_pr_ov_vi" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "ImageUrl", "ReleaseDate", "Title", "TrailerUrl" },
                values: new object[] { 2, "Francis Lawrence", 2, "Apocalyptic and post-apocalyptic fiction", "https://www.imdb.com/title/tt0480249/mediaviewer/rm2203650560/?ref_=tt_ov_i", new DateTime(2008, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "I Am Legend", "https://www.imdb.com/video/vi3820028185/?playlistId=tt0480249&ref_=tt_pr_ov_vi" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "ImageUrl", "ReleaseDate", "Title", "TrailerUrl" },
                values: new object[] { 3, "Keenen Ivory Wayans", 3, "Horror-Comedy", "https://www.imdb.com/title/tt0257106/mediaviewer/rm699865088/?ref_=tt_ov_i", new DateTime(2001, 7, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Scary Movie 2", "https://www.imdb.com/video/vi852532761/?playlistId=tt0257106&ref_=tt_ov_vi" });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharacterId",
                table: "CharacterMovie",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}

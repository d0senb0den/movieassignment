﻿using System.ComponentModel.DataAnnotations;

namespace MovieAssignmentAPI.Models
{
    public class Character
    {


        public Character()
        {
          
        }

        public int Id { get; set; }
        public virtual ICollection<Movie>? Movies { get; set; }

        [StringLength(50)]
        public string? Name { get; set; }

        [StringLength(50)]
        public string? Alias { get; set; }

        [StringLength(50)]
        public string? Gender { get; set; }

        [StringLength(300)]
        public string? ImageUrl { get; set; }
    }
}

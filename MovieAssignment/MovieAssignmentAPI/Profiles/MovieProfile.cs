﻿using AutoMapper;
using MovieAssignmentAPI.DTO.MovieDTO;
using MovieAssignmentAPI.Models;

namespace MovieAssignmentAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, DTO_ReadMovie>()
                .ForMember(movieDTO => movieDTO.Id, options => options
                .MapFrom(movie => movie.Id))
                .ForMember(movieDTO => movieDTO.Characters, options => options
                .MapFrom(movie => movie.Characters
                .Select(character => character.Id).ToList()))
                .ReverseMap();




            CreateMap<DTO_CreateMovie, Movie>()
               .ForMember(movie => movie.Characters, options => options
               .MapFrom(movieDTO => new List<Character>()))
               .ReverseMap();
        }
    }
}

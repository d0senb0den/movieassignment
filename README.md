# MovieAssignment

This is an ASP.NET Web API for movies, franchises and characters. Using CRUD, you can manipulate the endpoints, play around with the connections and manipulate data in a database (we used SSMS). The application runs in swagger where all the database functions are available. 

## Install
Clone the repository and open the solution using the sln file.
To run the project you would have to seed the database by typing the following command in the "Package Manager Console" in Visual Studio.
```
update-database
```
Done! Now you can simply run the project through Visual Studio, a web API server and a browser with Swagger will open automatically.

## Authors
#### [Johan Lantz (@johan.lantz.a)](@johan.lantz.a)
#### [Pontus Gillson (@d0senb0den)](@d0senb0den)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
